import csv
from operator import itemgetter

import pandas as pd
df = pd.read_csv('/home/maier/IDP_FirmClassification/output_table/themecloud/cloudstorage.themecloud.csv')

lists= df.values.tolist()
print ("How many rows-" + str(len(lists)))
all_tuple = []
all_count = []
if len(lists) > 12:
	print ("If condition")
	current = 0
	for l in reversed(lists):
		n_l = l[3:-1]
		zipped = zip (n_l[0::2], n_l[1::2])
		for t in zipped:
			if t[0] in all_tuple:
				index = all_tuple.index(t[0])
				all_count[index] += int (t[1])
			else:
				all_tuple.append(t[0])
				all_count.append(int (t[1]))
		current += 1
		if current == 12:
			break
else:
	print ("Else conddition")
	for l in lists:
		n_l = l[3:-1]
		zipped = zip (n_l[0::2], n_l[1::2])
		for t in zipped:
			if t[0] in all_tuple:
				index = all_tuple.index(t[0])
				all_count[index] += int(t[1])
			else:
				all_tuple.append(t[0])
				all_count.append(int (t[1]))
#print (all_tuple)
aggregated_tuple = zip(all_tuple, all_count)
sorted_aggregated_tuple = sorted(aggregated_tuple,key=itemgetter(1), reverse=True)
#print (sorted_aggregated_tuple)
if len(sorted_aggregated_tuple) > 10:
	sorted_aggregated_tuple = sorted_aggregated_tuple[0:10]
list_words = [ tp[0] for tp in sorted_aggregated_tuple]
print (list_words)
