'''
File name: ThemeFirmRelation.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''


from __future__ import division
from allocation.Comparision import SSSComparision
from operator import itemgetter
class ThemeFirmRelation(object):
	def __init__(self):
		self.theme_keywords = []
		self.firm_keywords = []
		self.relation_finder = SSSComparision()
	# returns how close two list of words using a score
	def get_relation(self, theme_keywords, firm_keywords):
		try:
			self.theme_keywords = theme_keywords
			self.firm_keywords = firm_keywords
			#print ("Get words ")		
			total_match = 0
			# for each word in the theme_keyword list computer similarity with each word from firm_keyword
			# accept best match
			for theme_keyword in self.theme_keywords:
				similarity_score_list = []
				for firm_keyword in self.firm_keywords:
					current_score = self.relation_finder.compare(theme_keyword, firm_keyword)
					if current_score >=0 and current_score <= 1:
						similarity_score_list.append(((theme_keyword, firm_keyword),current_score))
				# from the match find the best match for a word in the theme with a word in the firm
				matched_list = [ tupple for tupple in sorted(similarity_score_list,key=itemgetter(1), reverse=True)[:1] if tupple[1] > 0]
				# assign a score based on the highest match
				if len(matched_list) > 0:
				
					if matched_list[0][1] >= 0.35 and matched_list[0][1] < 0.4:
						#print '%s %s' % (matched_list[0][0], matched_list[0][1])
						#total_match += 0.10
						#total_match += 0.025
						total_match += 0.005
					elif matched_list[0][1] >= 0.4 and matched_list[0][1] < 0.45:
						#print '%s %s' % (matched_list[0][0], matched_list[0][1])
						#total_match += 0.20
						total_match += 0.010
					elif matched_list[0][1] >= 0.45 and matched_list[0][1] < 0.5:
						#print '%s %s' % (matched_list[0][0], matched_list[0][1])
						total_match += 0.03
					elif matched_list[0][1] >= 0.5 and matched_list[0][1] < 0.55:
						#print '%s %s' % (matched_list[0][0], matched_list[0][1])
						total_match += 0.05
					elif matched_list[0][1] >= 0.55 and matched_list[0][1] < 0.6:
						#print '%s %s' % (matched_list[0][0], matched_list[0][1])
						total_match += 0.07
					elif matched_list[0][1] >= 0.60 and matched_list[0][1] < 0.65:
						#print '%s %s' % (matched_list[0][0], matched_list[0][1])
						total_match += 0.15
					elif matched_list[0][1] >= 0.65 and matched_list[0][1] < 0.7:
						#print '%s %s' % (matched_list[0][0], matched_list[0][1])
						total_match += 0.25
					elif matched_list[0][1] >= 0.7 and matched_list[0][1] < 0.75:
						#print '%s %s' % (matched_list[0][0], matched_list[0][1])
						total_match += 0.35
					elif matched_list[0][1] >= 0.75 and matched_list[0][1] < 0.8:
						#print '%s %s' % (matched_list[0][0], matched_list[0][1])
						total_match += 0.50	
					elif matched_list[0][1] >= 0.8:
						#print '%s %s' % (matched_list[0][0], matched_list[0][1])
						total_match += 1
				

			# finally calculate how related the firm with the theme with following formula
			if (len(self.theme_keywords) > 0):	
				percent_match = (total_match/len(self.theme_keywords))*100
				return round(percent_match,2)
			else:
				return 0.0
		
		except Exception as e:
			raise Exception ("[Theme_Firm_relation][get_relation] "+ str(e))
		
