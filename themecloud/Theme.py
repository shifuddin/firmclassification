'''
File name: Theme.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

class Theme(object):
	def __init__(self, file_name):
		self.theme_file = open(file_name, 'r')
	
	# load list of themes 
	def load_theme_list(self):
		self.theme_list = self.theme_file.read().split('\n')
		if self.theme_list[len(self.theme_list)-1] == '':
			return self.theme_list[:-1]
		else:
			return self.theme_list
