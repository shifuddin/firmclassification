'''
File name:main_allocation.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

from __future__ import division
from allocation.ThemeFirmRelation import ThemeFirmRelation
from allocation.Firm import Firm
from common.DATE import DATE
from os import listdir
from os.path import isfile, join
from common.WordCloud import WordCloud
from common.WordCloudCSV import WordCloudCSV
from common.Log import Log
from allocation.WordCloudReader import WordCloudReader
from operator import itemgetter
import daemon
import schedule
import time
import sys
import json
'''
	change here if any change in project structure
'''
theme_directory = '/home/maier/IDP_FirmClassification/output_table/themecloud/'
firm_directory = '/home/maier/IDP_FirmClassification/output_table/firmcloud/'
allocation_directory = '/home/maier/IDP_FirmClassification/output_table/allocationcloud/'
log_directory = '/home/maier/IDP_FirmClassification/log/allocation/'
comparison_threshold_path = '/home/maier/IDP_FirmClassification/input/comparison_threshold.txt'
home_directory = '/home/maier/IDP_FirmClassification/firmclassification/'

def allocation_generation():
	date = DATE()
	# create configuration for log file 
	#logging.basicConfig(level=logging.ERROR, filename= log_directory + date.end_date+'.allocation.log', format='%(asctime)s %(message)s')
	log = Log(log_directory + date.end_date+'.allocation.log')
	logging = log.get_logger()
	try:
		# Read theme files and firm files
		theme_files = [f for f in listdir(theme_directory) if isfile(join(theme_directory, f))]
		firm_files = [f for f in listdir(firm_directory) if isfile(join(firm_directory, f))]
		
		# Create object of relation class, cloud reader and Firm
		theme_firm_realtion = ThemeFirmRelation()
		word_cloud_reader = WordCloudReader()
		firm_obj = Firm()
		# read setting file
		json_file = open(home_directory+sys.argv[1])
		settings_data = json.load(json_file)

		# take starting point from settings file
		theme_counter = settings_data["starting_allocation_position"]

		# for each theme, calculate the parcent match with each firm 
		# finally for each theme, write parcent match of all firms
		for i in range(theme_counter, len(theme_files)):
			# compute theme name
			theme_file = theme_files[i]
			theme_name = theme_file[:theme_file.find('.')]
			firm_percentage_list = []
			low_percentage_firm_list = []
			theme_cloud = word_cloud_reader.get_themecloud(theme_directory+theme_file)
			for firm_file in firm_files:
				# compute firm name
				firm_name = firm_file[:firm_file.find('.')]
				#print ('Comparing ' +theme_name+ ' with ' + firm_name)
				# compute score
				score = theme_firm_realtion.get_relation(theme_cloud, word_cloud_reader.get_firmcloud(firm_directory + firm_file))
				
				# store result
				# if score is more than 24 added to one list else another list
				try:
					if int(score) > settings_data['match_threshold']:
						firm_percentage_list.append((firm_name,firm_obj.get_ticker(firm_name),  score))
					else:
						low_percentage_firm_list.append((firm_name,firm_obj.get_ticker(firm_name), score))
				except Exception as e:
					logging.error('NaN Score ' + str(e))
			# if firm percentage list does not contain more than 3, I associate low percentage list with theme
			# unless associate firm percentage
			if len(firm_percentage_list) > 0:
				allocation_cloud = WordCloud(theme_name,date.end_date, -1, sorted(firm_percentage_list,key=itemgetter(2), reverse=True) )
			else:
				# in case low percentage firms, just added first ten
				#combined_list = firm_percentage_list + low_percentage_firm_list
				sorted_low_percentage_list = sorted(low_percentage_firm_list, key=itemgetter(2), reverse=True)
				
				try:
					sliced_list = sorted_low_percentage_list[0:settings_data['below_threshold']]
					#print (setting)
				except Exception as e:
					sliced_list = sorted_low_percentage_list
				# create object of wordcloud.	
				allocation_cloud = WordCloud(theme_name,date.end_date, -1, sliced_list )
			try:
				# write object into wordcloud csv
				allocation_cloud_csv = WordCloudCSV(allocation_directory + theme_name+".allocationcloud", 'null')
				allocation_cloud_csv.append(allocation_cloud)
				theme_counter = theme_counter + 1
				logging.info(theme_name + " is finised ( " + str(theme_counter)+" of "+str(len(theme_files))+")")
			except Exception as e:
				logging.error(str(e))
	except Exception as e:
		logging.error("[main_allocation_cloud.py] "+ str(e))

'''
schedule cloud generation at particular time
'''
def do_something():
	schedule.every().tuesday.at("13:00").do(allocation_generation)
	while True:
		schedule.run_pending()
		time.sleep(1)
'''
create daemon context so that next task will be executed as service
'''
def run():
	with daemon.DaemonContext():
		do_something()

if __name__ == "__main__":
	run()
	#allocation_generation()
