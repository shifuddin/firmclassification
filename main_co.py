'''
File name: main_co.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

from common.RSSFeed import RSSFeed
from common.Article import Article
from common.DATE import DATE
from common.TermDocumentMatrix import TermDocumentMatrix
from common.WordCloud import WordCloud
from common.WordCloudCSV import WordCloudCSV
from common.Log import Log
import sys
import daemon
import time
import schedule
import json

'''
	change here if any change in project structure
'''

log_directory = '/home/maier/IDP_FirmClassification/log/cooccurance/'
input_firm_list = '/home/maier/IDP_FirmClassification/input/firm_list.txt'
output_directory = '/home/maier/IDP_FirmClassification/output_table/'
ticker_file = '/home/maier/IDP_FirmClassification/input/ticker_list.txt'
shortname_file = '/home/maier/IDP_FirmClassification/input/shortname_list.txt'
home_directory = '/home/maier/IDP_FirmClassification/firmclassification/'

def cloud_generation():

	frequecy_distribution_type = 'co'
	date = DATE()
	# crate log cofig
	log = Log(log_directory + date.end_date+"."+frequecy_distribution_type+'.log')
	logging = log.get_logger()

	try:		
		# create firm list from firm file
		firm_list_file = open(input_firm_list, encoding='utf-8', errors='ignore')
		firm_list_file_content = firm_list_file.readlines()

		firm_list = [ line.strip().lower() for line in firm_list_file_content]
		
		# read ticker and shortname
		ticker_list_file = open(ticker_file, encoding='utf-8', errors='ignore')
		file_content = ticker_list_file.readlines()
		ticker_list = [ line.strip().lower() for line in file_content]

		shortname_list_file = open(shortname_file,encoding='utf-8', errors='ignore')
		file_content = shortname_list_file.readlines()
		shortname_list = [ line.strip().lower() for line in file_content]
		
		# create term document matrix and date obj
		tdm = TermDocumentMatrix(ticker_list, shortname_list, [])
		date = DATE()
	
		# read setting file
		json_file = open(home_directory+sys.argv[1])
		settings_data = json.load(json_file)

		# take starting point from settings file
		count = settings_data["starting_firm_position"]		
	
		# for each firm in the firm list create co-occurrence cloud
		for i in range(count, len(firm_list)):
			firm = firm_list[i]
			ticker = firm.split('\t')[0].lower()
			shortname = firm.split('\t')[1].lower()
			
			# from firm name extract ticker and create rss feed
			rss_feed = RSSFeed('https://feeds.finance.yahoo.com/rss/2.0/headline?s=', ticker, date.end_date)
			article_text_list = []
	
			# collect text from all articles in the rss feed
			for article_links in rss_feed.get_articles():
				article = Article(article_links)
				article_text_list.append(article.get_text())
		
			frequecy_distribution = []
			
			output_type = 'cooccurancecloud'
			
			# create frequency distribution of co-occurred firms
			frequecy_distribution = tdm.get_cooccured_firms_frequency_distribution(article_text_list, ticker, shortname, settings_data['first_n_freq'])
			
			# create frequency from frequency distribution
			word_cloud = WordCloud(shortname, date.end_date, str(len(article_text_list)), frequecy_distribution)
			
			# write cloud into csv file
			try:
				firm_cloud_csv = WordCloudCSV (output_directory+ output_type+'/' + shortname+'.cocloud', 'null')
				firm_cloud_csv.append(word_cloud)
			except Exception as e:
				logging.error(str(e))

			count += 1
			logging.info(shortname+ " finished ("+ str(count) + " of " +str((len(firm_list))) +" )")
	except Exception as e:
		logging.error("[main_co_cloud] "+ str(e))
	
# schedule cloud generation at particular time of week
def do_something():
	schedule.every().saturday.at("16:00").do(cloud_generation)
	while True:
		schedule.run_pending()
		time.sleep(1)

# create daemon context to make the program service
def run():
	with daemon.DaemonContext():
		do_something()

if __name__ == "__main__":
	run()
	#cloud_generation()
