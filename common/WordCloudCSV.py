'''
File name: WordCloudCSV.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

import csv
import os.path
csv.field_size_limit(500 * 1024 * 1024)
class WordCloudCSV(object):
	def __init__(self, csv, text):
		self.file_name = csv + '.csv'
		self.text = text
	def append(self, word_cloud):
		# create headers and add headers in case of new file
		heading_list = []
		word_cloud_list_style = []
		word_cloud_list_style.append(word_cloud.firm_name)
		heading_list.append("Theme/Firm")
		word_cloud_list_style.append(word_cloud.date)
		heading_list.append("Date")
		keyword_column_one = 'Firm shortname'
		keyword_column_two = 'Firm ticker'
		value_column_name = 'Percent Match'
		# if we have count then add another column named no of tweets / article
		if word_cloud.no_articles != -1:
			word_cloud_list_style.append(word_cloud.no_articles)
			heading_list.append("No. Tweet/Article")
			keyword_column_one = 'Keyword'
			value_column_name = 'Count'
		# add header keyword and count as many time we have dictionary in freq dist.
		# add value of dictionary into list
		for dic in word_cloud.frequency_distribution:
			# in case of allocation cloud this condition works
			if len(dic) == 3:
				word_cloud_list_style.append(dic[0])
				heading_list.append(keyword_column_one)
				word_cloud_list_style.append(dic[1])
				heading_list.append(keyword_column_two)
				word_cloud_list_style.append(dic[2])
				heading_list.append(value_column_name)
			else:
				word_cloud_list_style.append(dic[0])
				heading_list.append(keyword_column_one)
				word_cloud_list_style.append(dic[1])
				heading_list.append(value_column_name)

		# add full text to list in case of not null value also add a header for that
		if self.text != 'null':
			heading_list.append('Full Text')
			word_cloud_list_style.append(self.text)
		# if new file, add header to csv then list of content
		if os.path.isfile(self.file_name) == False:
			self.csv_file = open(self.file_name, 'a+')
			writer = csv.writer(self.csv_file, quoting=csv.QUOTE_ALL)
			writer.writerow(heading_list)
			writer.writerow(word_cloud_list_style)
			self.csv_file.close()
		# if already exists then add only list of content
		else:
			self.csv_file = open(self.file_name, 'a+')
			writer = csv.writer(self.csv_file, quoting=csv.QUOTE_ALL)
			writer.writerow(word_cloud_list_style)
			self.csv_file.close()
