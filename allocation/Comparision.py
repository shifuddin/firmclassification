'''
File name: Comparision.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

import wordninja
import enchant
from requests import get
from nltk.corpus import wordnet
import Levenshtein
class SSSComparision(object):
	def __init__(self):
		# create variable for sim service semantic similarity which is unavailable now 
		self.dictionary = enchant.Dict("en_US")
		self.sss_url = "http://swoogle.umbc.edu/SimService/GetSimilarity"
		self.sss_type = 'relation'
		self.sss_corpus = 'webbase'
	# check valid english word
	def valid_eng_word(self,word):
		try:
			if self.dictionary.check(word) == True:
				return True
			else:
				return False
		except Exception as e:
			raise Exception ("[Comparison][valid_eng_word] " +str(e))
	# split long text into meaningful words, space seperated
	def split_word(self, word):
		try:

			return " ".join(wordninja.split(word))
		except Exception as e:
			raise Exception ("[Comparison][split_word] "+ str(e))
	# check if any word is valid english word
	def valid_words_after_split(self, word):
		try:
			word_list = wordninja.split(word)
			for word_1 in word_list:
				if len(word_1) == 1:
					return False
			return True
		except Exception as e:
			return False
	# compare two words semantic similarity using sim service similarity
	def compare_algo_sim_service(self, s1, s2):
		try:
			response = get(self.sss_url, params={'operation':'api','phrase1':s1,'phrase2':s2,'type':self.sss_type,'corpus':self.sss_corpus})
		 	#print '%s, %s' % (s1,s2)
			return float(response.text.strip())		
		except Exception as e:
			raise Exception ("[Comparison][compare_algo_sim_service] "+ str(e))
	# compare two words using wordnet semantic similarity
	def compare_algo_wordnet(self, s1, s2):
		
		try:
			l_ratio = Levenshtein.ratio(s1, s2)
			if l_ratio == 1.0:
				return 1.0
			wordFromList1 = wordnet.synsets(s1)
			wordFromList2 = wordnet.synsets(s2)
			s = 0
			if wordFromList1 and wordFromList2: #Thanks to @alexis' note
				s = wordFromList1[0].wup_similarity(wordFromList2[0])
			if s is None:
				#print ("oka")
				return 0
			return s
		except Exception as e:
			raise Exception ("[Comparison][compare_algo_wordnet] "+ str(e))
	
	# compare two words and return compare value
	def compare(self, s1, s2):
		try:
			if self.valid_words_after_split (s1) == True:
					s1 = self.split_word(s1)
			if self.valid_words_after_split (s2) == True:
					s2 = self.split_word(s2)
			response = 0
			try:
				response = self.compare_algo_wordnet(s1, s2)
				#print ("%s %s %s" %(s1, s2, response))
				return response
			except:
				#print ('Error in getting similarity for %s: %s' % ((s1,s2), response))
				return 0.0
		except Exception as e:
			raise Exception ("[Compare][compare] "+ str(e))
			
