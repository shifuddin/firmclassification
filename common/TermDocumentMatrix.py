'''
File name: TermDocumentMatrix.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

import textmining
import nltk
from nltk.stem.snowball import SnowballStemmer
from operator import itemgetter
from nltk.stem import WordNetLemmatizer

class TermDocumentMatrix (object):
	def __init__(self, ticker_list, shortname_list, common_word_list):
		self.frequency_distribution = []	
		self.firm_ticker_list = ticker_list

		self.firm_shortname_list = shortname_list
		self.common_word_list = common_word_list
		self.wordnet_lemmatizer = WordNetLemmatizer()
	
	# tokenize text, create lemma from each token and join to a string	
	def tokenize_and_lemma(self, text):
		tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
		lemmas = [self.wordnet_lemmatizer.lemmatize(t) for t in tokens]
		lemmas_string = ' '.join(lemmas)
		return lemmas_string
	# tokenize text, tagging token, filter token and join tokens which are in the shortname list
	def tokenize_and_tagging(self, text):
		tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
		tagged_tokens = nltk.pos_tag(tokens)
		NOUN_tagged_tokens = [tagged_token[0] for tagged_token in tagged_tokens if tagged_token[1] == "NN" or tagged_token[1] == "NNS" or tagged_token[1] == "NNP"]
		words_in_shortname_list = [tagged_token for tagged_token in NOUN_tagged_tokens if tagged_token.lower() in self.firm_shortname_list]
		tokens_string = ' '.join(words_in_shortname_list)
		return tokens_string
		
	def get_keywords_frequency_distribution (self, article_text_list, first_n_freq):
		# Initialize class to create term-document matrix
		tdm = textmining.TermDocumentMatrix()
		# Add the documents
		for doc in article_text_list:
			tdm.add_doc(self.tokenize_and_lemma(doc))
		
		# this will hold tdm as array
		tdm_array = []
		# list of features
		words_in_tdm = []
	
		counter = 0
		# copy value of term document matrix into tdm array
		for row in tdm.rows(cutoff=1):
			if counter != 0:
				tdm_array.append(row)
			else:
				words_in_tdm = row
			counter += 1
	
		frequency_in_all_documents = []
		# sum each column of array to find frequency of each word
		for i in range (0,len(words_in_tdm)):
			sum_all_document = 0
			for j in range(0,counter -1):
				sum_all_document += tdm_array[j][i]
			frequency_in_all_documents.append(sum_all_document)
	
	
		# tagged words in tdm
		tagged_words_in_tdm = nltk.pos_tag(words_in_tdm)
		# create frequency dist
		frequency_distribution = zip(tagged_words_in_tdm, frequency_in_all_documents)
		# filter based on noun
		filtered_frequency_distribution = [(tupple[0][0], tupple[1]) for tupple in frequency_distribution if (tupple[0][1] == "NN" or tupple[0][1] == "NNS" or tupple[0][1] == "NNP")]
		# filter which are not in ticker list
		ticker_frequency_distribution = [tupple for tupple in filtered_frequency_distribution if tupple[0].lower() not in self.firm_ticker_list]
		# filter which are not in shortname list
		shortname_frequency_distribution = [tupple for tupple in ticker_frequency_distribution if tupple[0].lower() not in self.firm_shortname_list]
		# remove common words
		eng_frequency_distribution = [tupple for tupple in filtered_frequency_distribution if tupple[0].lower() not in self.common_word_list]
		# sort and slice first 40
		sorted_eng_frequency_distribution = sorted(eng_frequency_distribution,key=itemgetter(1), reverse=True)
		try:
			sliced_frequency_distribution = sorted_eng_frequency_distribution[0:first_n_freq]
		except Exception as e:
			sliced_frequency_distribution = sorted_eng_frequency_distribution
		
		self.frequency_distribution = sliced_frequency_distribution
		
		# return sliced frequency
		return self.frequency_distribution
		
	def get_cooccured_firms_frequency_distribution (self, article_text_list, ticker, shortname, first_n_freq):
		
		# Initialize class to create term-document matrix
		tdm = textmining.TermDocumentMatrix()
		# Add the documents
		for doc in article_text_list:
			tdm.add_doc(self.tokenize_and_tagging(doc))
		
		# this will hold tdm as array
		tdm_array = []
		# list of features
		words_in_tdm = []
	
		counter = 0
	
		# see comments from previous method
		for row in tdm.rows(cutoff=1):
			if counter != 0:
				tdm_array.append(row)
			else:
				words_in_tdm = row
			counter += 1
	
		frequency_in_all_documents = []
		
		for i in range (0,len(words_in_tdm)):
			sum_all_document = 0
			for j in range(0,counter -1):
				sum_all_document += tdm_array[j][i]
			frequency_in_all_documents.append(sum_all_document)
	
	
		# create frequency dist
		frequency_distribution = zip(words_in_tdm, frequency_in_all_documents)
		# remove current firm 
		filtered_frequency_distribution = [(tupple[0].upper(),tupple[1]) for tupple in frequency_distribution if tupple[0] != shortname and tupple[0] != ticker ]
		# short and slice first 40 firms
		sorted_frequency_distribution = sorted(filtered_frequency_distribution,key=itemgetter(1), reverse=True)
		
		try:
			sliced_frequency_distribution = sorted_frequency_distribution[0:first_n_freq]
		except Exception as e:
			sliced_frequency_distribution = sorted_frequency_distribution
		
		self.frequency_distribution = sliced_frequency_distribution
		# return distribution
		return self.frequency_distribution
