'''
File name: WordCloud.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

from collections import Counter

class WordCloud(object):
	# create object using parameter values
	def __init__(self, firm_name, date, no_articles, frequency_distribution):
		self.firm_name = firm_name
		self.date = date
		self.no_articles = no_articles
		self.frequency_distribution = frequency_distribution
		
