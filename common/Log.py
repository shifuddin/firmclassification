import logging
class Log(object):
	def __init__(self, path):
		self.logger = logging.getLogger(__name__)
		self.logger.setLevel(logging.INFO)

 		# create a file handler
		handler = logging.FileHandler(path)
		handler.setLevel(logging.INFO)
		# create a logging format
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		handler.setFormatter(formatter)
		# add the handlers to the logger
		self.logger.addHandler(handler)
	def get_logger(self):
		return self.logger
