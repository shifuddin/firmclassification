'''
File name: ThemewordCLoud.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

from themecloud.Tweet import Tweet
from collections import Counter
from operator import itemgetter
import numpy as np
import math
class ThemewordCloud(object):

	def __init__(self, theme, start_date, end_date):
		self.firm_name = theme
		self.start_date = start_date
		self.date = end_date
	
	# generate cloud map
	def generate_cloud_map(self, first_n):
		try:
			# create object of tweet class
			tweetObj = Tweet(self.start_date, self.date)
			
			# call getTweets to have list of keywords in the tweets
			tweets = tweetObj.getTweets(self.firm_name, 1800)
			
			self.no_articles = tweets[0]
			
			# create frequency distribution from keywords
			frequency_distribution = sorted(Counter(tweets[1]).items(),key=itemgetter(1), reverse=True)
			
			# slice highest frequenct keywords if not available then all
			try:
				if len(frequency_distribution) > first_n:
					sliced_frequency_distribution = frequency_distribution[0:first_n]
				else:
					sliced_frequency_distribution = frequency_distribution
			except Exception as e:
				raise Exception ("[Themewordcloud][generate_cloud_map][inner try block] "+ str(e))
			
			# add text, frequency distribution and number of tweets into self
			self.frequency_distribution = sliced_frequency_distribution
			self.text = tweets[2]
		except Exception as e:
			raise Exception ("[Themewordcloud][generate_cloud_map][outer try block] "+ str(e))
		# return self
		return self
		
