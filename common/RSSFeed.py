'''
File name: RSSFeed.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

import xml.etree.ElementTree as ET
import urllib
class RSSFeed (object):
	def __init__(self, base_url, search_key, pub_date):
		self.search_key = search_key
		self.base_url = base_url
		self.pub_date = pub_date
		self.article_link_list = []
	def get_articles(self):
		# create feed url 
		url = self.base_url + self.search_key+'&region=US&lang=en-US'
		req = urllib.request.Request(url)
		try:
			response = urllib.request.urlopen(req)
			rss_feed_string = response.read()
			
			# parse feed xml
			root = ET.fromstring(rss_feed_string)
			
			element = root.find('channel')
			
			# add each article link to list and return
			for item in element.findall('item'):
				link = item.find('link').text
				self.article_link_list.append(link)
			return self.article_link_list
			
		except Exception as e:
			print (e)
		
		
