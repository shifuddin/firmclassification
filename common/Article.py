'''
File name: Article.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

import urllib
from readability.readability import Document
from bs4 import BeautifulSoup
class Article (object):
	
	def __init__(self, url):
		self.url = url
		self.text = ''
		# create request object from url
		req = urllib.request.Request(self.url)
		try:
			# get response from url
			response = urllib.request.urlopen(req)
			# only read html
			html = response.read()
			# body part from html
			readable_article = Document(html).summary()
			# title from html
			readable_title = Document(html).title()
			# read text from body
			soup = BeautifulSoup(readable_article,'lxml')
			self.text = soup.text
			
		except Exception as e:
			print ("Article object creation problem")
	def get_text(self):
		# return text
		return self.text
		
		
		
