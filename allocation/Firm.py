
'''
File name: Firm.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 15/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

import sys
class Firm(object):
	def __init__(self):
		firm_file = "/home/maier/IDP_FirmClassification/input/firm_list.txt"
		with open(firm_file, 'r') as f:
        		self.content_list = f.readlines()
	def get_ticker(self, shortname):
		try:
			for content in self.content_list:
        			content_array = content.split('\t')
        			content_ticker = content_array[0]
        			content_shortname = content_array[1][:-1]
			        #print (shortname + "\t\t"+ content_shortname)
        			if shortname == content_shortname.lower():
                			return content_ticker
			return "No ticker"
		except Exception as e:
			raise Exception ("[Firm][get_ticker] "+str(e))

