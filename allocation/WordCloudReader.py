'''
File name: WordCloudReader.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

import csv
import pandas as pd
from operator import itemgetter
class WordCloudReader(object):
	def __init__(self):
		self.cloud_key = ''
		self.cloud_values = []
	# read last row of a csv file starting from 3rd column and ignore each second
	# escape full text
	# returns values as list
	def get_firmcloud(self, filename):
		try:
			with open(filename) as csvfile:
				reader = csv.reader(csvfile)
				last = list(reader)[-1][:-1]
		
				self.cloud_values = [word for word in last[3::2]]
		
			return self.cloud_values
		except Exception as e:
			raise Exception ("[Class: Word_cloud_reader Method:get_firmcloud] "+ str(e))
		
	def get_themecloud(self, filename):
		try:
			my_cols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y"]
			df = pd.read_csv(filename, names=my_cols,  engine='python')
			df_lists= df.values.tolist()
			df_lists.pop(0)
			words = []
			count = []
		
			if len(df_lists) > 12:
				current = 0
				for l in reversed(df_lists):
					n_l = l[3:-1]
					zipped = zip (n_l[0::2], n_l[1::2])
					for t in zipped:
						keyword_count = 0
						if isinstance(t[1], int):
							keyword_count = int(t[1])
						if t[0] in words:
							index = words.index(t[0])
							count[index] += keyword_count
						else:
							words.append(t[0])
							count.append(keyword_count)
					current += 1
					if current == 12:
						break
			else:	
				for l in df_lists:
					n_l = l[3:-1]
					zipped = zip (n_l[0::2], n_l[1::2])
					for t in zipped:
						keyword_count = 0
						if isinstance(t[1], int):
							keyword_count = int(t[1])
						if t[0] in words:
							index = words.index(t[0])
							count[index] += keyword_count
						else:
							words.append(t[0])
							count.append(keyword_count)
			aggregated_tuple = zip(words, count)
			sorted_aggregated_tuple = sorted(aggregated_tuple,key=itemgetter(1), reverse=True)

			if len(sorted_aggregated_tuple) > 10:
		        	sorted_aggregated_tuple = sorted_aggregated_tuple[0:10]
		
			self.cloud_values = [ tp[0] for tp in sorted_aggregated_tuple]
			return self.cloud_values
		except Exception as e:
			raise Exception ("[Word_Cloud_reader][get_themecloud] "+str(e))

