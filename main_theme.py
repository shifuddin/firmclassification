'''
File name: main_theme.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

from themecloud.Tweet import Tweet
from themecloud.Themewordcloud import ThemewordCloud
from themecloud.Theme import Theme
from common.DATE import DATE
from common.Log import Log
import daemon
import time
import schedule
from common.WordCloudCSV import WordCloudCSV
import sys
import json

'''
	change here, if any change in project structure
'''
theme_directory = '/home/maier/IDP_FirmClassification/output_table/themecloud/'
input_theme_list = '/home/maier/IDP_FirmClassification/input/themelist.txt'
log_directory = '/home/maier/IDP_FirmClassification/log/theme/'
home_directory = '/home/maier/IDP_FirmClassification/firmclassification/'


def cloud_generation ():
	
	# create object of theme class
	theme_obj = Theme(input_theme_list)
	# load theme list from theme class
	theme_list = theme_obj.load_theme_list()

	# create date obj
	dateObj = DATE()
	# create config for log file
	log = Log(log_directory + dateObj.end_date+'.theme.log')
	logging = log.get_logger()

	# read setting file
	json_file = open(home_directory+sys.argv[1])
	settings_data = json.load(json_file)

	# take starting point from settings file
	current = settings_data["starting_theme_position"]
	for i in range(current, len(theme_list)):
		theme = theme_list[i]
		# create theme word cloud obj
		themewordcloud_obj = ThemewordCloud(theme, dateObj.start_date, dateObj.end_date)
		# update the object with necessary information 
		themewordcloud_obj = themewordcloud_obj.generate_cloud_map(settings_data['first_n_freq'])
		# write word cloud obj into cloud csv
		try:
			theme_cloud_csv = WordCloudCSV(theme_directory + theme+".themecloud", themewordcloud_obj.text)
			theme_cloud_csv.append(themewordcloud_obj)
		except Exception as e:
			logging.error(str(e))

		current += 1
		logging.info(theme + " finished ( "+ str(current) + " of " + str(len(theme_list)) +" )")

# schedule cloud generation at particular time
def do_something():
	schedule.every().saturday.at("16:00").do(cloud_generation)
	while True:
		schedule.run_pending()
		time.sleep(1)

# create daemon context for service
def run():
	try:
		with daemon.DaemonContext():
			do_something()
	except Exception as e:
		print (str(e))

if __name__ == "__main__":
	run()
	#cloud_generation()
