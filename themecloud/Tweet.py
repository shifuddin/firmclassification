'''
File name: main_allocation.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''


import got3 as got
from langdetect import detect
from nltk.stem import WordNetLemmatizer
class Tweet(object):
	def __init__(self,startDate, endDate):
		self.startDate = startDate
		self.endDate = endDate
	# check whether the text is english using langdetect	
	def isEnglish(self, text):
		try:
			return detect(text)
		except Exception:
			return 'null'
	# extract hashtags from string of hashtags and insert into hashtaglish
	def mergeHashtags(self, hashtaglist, hashtagstring):
		currenttaglist = hashtagstring.split()
		for hashtag in currenttaglist:
			if hashtag not in hashtaglist:
				hashtaglist.append(hashtag.lower())
		return hashtaglist
	
	# search twitter with search query and generate keywords
	def getTweets(self, searchQuery, number):
		# build tweet criteria
		tweetCriteria = got.manager.TweetCriteria().setQuerySearch(searchQuery).setSince(self.startDate).setUntil(self.endDate).setMaxTweets(number)
		
		tweets = []
		try:
			#get tweets
			tweets = got.manager.TweetManager.getTweets(tweetCriteria)
		
		except Exception:
			getTweets(searchQuery, number)

		tweetInfo = []
		try:
			
			tweetHashtagsList = []
			number_of_tweets = 0
			# create lemmatizer
			wordnet_lemmatizer = WordNetLemmatizer()
			
			tweetTextList = []
			# for each tweet in tweet list take the hashtags checks, lemmatize and add to keyword list
			for tweet in tweets:
				if self.isEnglish(tweet.text) == 'en':
					#log.write(" text "+tweet.text+'\n')
					number_of_tweets += 1
					tweet_hashtag_list = tweet.hashtags.encode('ascii', 'ignore').decode().split()
				
					# add text. It was a add request
					tweetTextList.append(tweet.text)
					
					for hashtag in tweet_hashtag_list:
						#log.write("Hash tag "+hashtag)
						if hashtag[1:] !='':
							tweetHashtagsList.append(wordnet_lemmatizer.lemmatize(hashtag[1:].lower()))
		
								
			
			tweetInfo.append(str(number_of_tweets))
			tweetInfo.append(tweetHashtagsList)
			tweetInfo.append(' . '.join(tweetTextList))
			
		except Exception as e:
			raise Exception ("[Tweet][get_tweets] "+ str(e))
		# return tweetinfo which contains number of tweets, hashtag list as keyword and text from all tweets
		return tweetInfo
