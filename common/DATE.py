'''
File name: DATE.py
Author: Md Shifuddin Al Masud
Python Version: 3.6
Last Modified: 14/11/2017
Email: shifuddin.masud@gmail.com
Version: 1.0.1
Copyright 2017, Md Shifuddin Al Masud
License: GPLv3
'''

import datetime
class DATE (object):
	# create date object with two member one is current data and another one is date one week back
	def __init__(self):
		today = datetime.datetime.today().strftime('%Y-%m-%d')
		week_ago = datetime.datetime.today() - datetime.timedelta(days=7)
		week_ago = week_ago.strftime('%Y-%m-%d')
		self.start_date = week_ago
		self.end_date = today
